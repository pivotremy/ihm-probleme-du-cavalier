﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.Collections;

namespace IHM
{
    public partial class Game : Form
    {
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem m_exitToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem m_aboutToolStripMenuItem;
        private ToolStripMenuItem m_ruleToolStripMenuItem;
        private Button m_cancelButton;
        private Button m_solutionButton;
        private Button m_RandomButton;
        private Button m_ResetButton;
        private Label label1;

        public class ButtonGame : Button
        {
            int m_line;
            int m_column;
            public int getline()
            {
                return m_line;
            }
            public int getColumn()
            {
                return m_column;
            }
            public ButtonGame(int m_line, int m_column)
            {
                this.m_column = m_column;
                this.m_line = m_line;
            }
        }

        static int[] depi = new int[] { 2, 1, -1, -2, -2, -1, 1, 2 };
        static int[] depj = new int[] { 1, 2, 2, 1, -1, -2, -2, -1 };
        ButtonGame[,] echec;
        int cmpt;
        int m_life;
        Color color_w = Color.White;
        Color color_b = Color.BlueViolet;
        Stack<ButtonGame> m_shot_played;
        Image cavalier;

        public Game()
        {
            InitializeComponent();
        }
        private void Game_Load(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.menuStrip1                         = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem              = new System.Windows.Forms.ToolStripMenuItem();     
            this.m_exitToolStripMenuItem               = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem                  = new System.Windows.Forms.ToolStripMenuItem();
            this.m_aboutToolStripMenuItem                 = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ruleToolStripMenuItem                = new System.Windows.Forms.ToolStripMenuItem();
            this.m_cancelButton                                = new System.Windows.Forms.Button();
            this.m_solutionButton                                = new System.Windows.Forms.Button();
            this.m_RandomButton                                = new System.Windows.Forms.Button();
            this.m_ResetButton= new System.Windows.Forms.Button();
            this.label1                                 = new System.Windows.Forms.Label();

            this.menuStrip1.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.fileToolStripMenuItem,this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(982, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";

 

            // m_exitToolStripMenuItem
            // 
            this.m_exitToolStripMenuItem.Name = "m_exitToolStripMenuItem";
            this.m_exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.m_exitToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.m_exitToolStripMenuItem.Text = "Close";
            this.m_exitToolStripMenuItem.Click += new System.EventHandler(this.m_exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_aboutToolStripMenuItem,
            this.m_ruleToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // m_aboutToolStripMenuItem
            // 
            this.m_aboutToolStripMenuItem.Name = "m_aboutToolStripMenuItem";
            this.m_aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.m_aboutToolStripMenuItem.Text = "About";
            this.m_aboutToolStripMenuItem.Click += new System.EventHandler(this.m_aboutToolStripMenuItem_Click);
            // 
            // m_ruleToolStripMenuItem
            // 
            this.m_ruleToolStripMenuItem.Name = "m_ruleToolStripMenuItem";
            this.m_ruleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.m_ruleToolStripMenuItem.Text = "Rule";
            this.m_ruleToolStripMenuItem.Click += new System.EventHandler(this.m_ruleToolStripMenuItem_Click);
            // 
            // m_cancelButton
            // 
            this.m_cancelButton.BackColor = System.Drawing.Color.Gray;
            this.m_cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_cancelButton.Font = new System.Drawing.Font("Charlemagne Std", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_cancelButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_cancelButton.Location = new System.Drawing.Point(5*60-30,10+9*60);
            this.m_cancelButton.Name = "m_cancelButton";
            this.m_cancelButton.Size = new System.Drawing.Size(150, 50);
            this.m_cancelButton.TabIndex = 1;
            this.m_cancelButton.Text = "cancel previous movement";
            this.m_cancelButton.UseVisualStyleBackColor = false;
            this.m_cancelButton.Click += new System.EventHandler(this.m_cancelButton_Click);
            // 
            // m_solutionButton
            // 
            this.m_solutionButton.BackColor = System.Drawing.Color.Gray;
            this.m_solutionButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_solutionButton.Font = new System.Drawing.Font("Charlemagne Std", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_solutionButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_solutionButton.Location = new System.Drawing.Point(60,10+60*9);
            this.m_solutionButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.m_solutionButton.Name = "m_solutionButton";
            this.m_solutionButton.Size = new System.Drawing.Size(100, 50);
            this.m_solutionButton.TabIndex = 2;
            this.m_solutionButton.Text = "See the solution";
            this.m_solutionButton.UseVisualStyleBackColor = false;
            this.m_solutionButton.Click += new System.EventHandler(this.m_solutionButton_Click);


            this.m_ResetButton.BackColor = System.Drawing.Color.Gray;
            this.m_ResetButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_ResetButton.Font = new System.Drawing.Font("Charlemagne Std", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ResetButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_ResetButton.Location = new System.Drawing.Point(60*3-15, 10 + 60 * 9);
            this.m_ResetButton.Name = "Reset";
            this.m_ResetButton.Size = new System.Drawing.Size(100, 50);
            this.m_ResetButton.TabIndex = 2;
            this.m_ResetButton.Text = "Reset";
            this.m_ResetButton.UseVisualStyleBackColor = false;
            this.m_ResetButton.Click += new System.EventHandler(this.ButtonReset_Clik);
         
            // m_RandomButton
            this.m_RandomButton.BackColor = System.Drawing.Color.Gray;
            this.m_RandomButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_RandomButton.Font = new System.Drawing.Font("Charlemagne Std", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_RandomButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_RandomButton.Location = new System.Drawing.Point(60*7+20,10+60*9);
            this.m_RandomButton.Name = "m_RandomButton";
            this.m_RandomButton.Size = new System.Drawing.Size(100, 50);
            //this.m_RandomButton.TabIndex = 3;
            this.m_RandomButton.Text = "Random";
            this.m_RandomButton.UseVisualStyleBackColor = false;
            this.m_RandomButton.Click += new System.EventHandler(this.m_RandomButton_Click);
            // label1
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Ebrima", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(250, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 32);
            // Form1
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_ResetButton);
            this.Controls.Add(this.m_RandomButton);
            this.Controls.Add(this.m_solutionButton);
            this.Controls.Add(this.m_cancelButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Game";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            echec = new ButtonGame[12, 12];
            cmpt = 0;
            m_life = 5;
            displayLife();
            m_shot_played = new Stack<ButtonGame>();
           cavalier = Image.FromFile(@"Pictures\CavalierR.jpg");
            m_solutionButton.Visible = false;
            m_cancelButton.Visible = false;
            m_ResetButton.Visible = false;

            for (int i = 0; i < 12; i++)
                for (int j = 0; j < 12; j++)
                {
                    ButtonGame b = new ButtonGame(i, j);
                    if (i < 2 | i > 9 | j < 2 | j > 9)
                    {
                        b.Enabled = false;
                    }
                    else
                    {
                        this.Controls.Add(b);
                        Point p = new Point(60*(i-1),60 *(j-1));
                        b.Location = p;
                        Size s = new Size(60, 60);
                        b.Size = s;
                        b.Click += new EventHandler(this.Boutons_Click);
                        Boolean claire;
                        claire = i % 2 == j % 2 ? true : false;
                        b.BackColor = claire ? color_w : color_b;
                        b.Font = new Font("Impact", 15);
                    }
                    echec[i, j] = b;
                }
            
        }
        private void ColorClear()
        {
            if (m_shot_played.Count == 0)
                return;
            ButtonGame b = m_shot_played.Peek();
            int ligne = b.getline();
            int col = b.getColumn();
            b.BackgroundImage = null;
            Boolean claire = b.BackColor.Equals(color_w);
            for (int l = 0; l < depj.Length; ++l)
            {
                ButtonGame next = echec[ligne + depi[l], col + depj[l]];
                if (next.Enabled)
                    next.BackColor = claire ? color_b : color_w;
            }
        }
        private void PossibleCases()
        {
            ButtonGame b = m_shot_played.Peek();
            int ligne = b.getline();
            int col = b.getColumn();
            int n = 0;
            for (int l = 0; l < depj.Length; ++l)
            {
                ButtonGame next = echec[ligne + depi[l], col + depj[l]];
                if (next.Enabled)
                {
                    next.BackColor = Color.Red;
                    ++n;
                }
            }
            if (n == 0)
            {
                String s = cmpt != 64 ? "You lost :(" : this.Text.Equals(" ") ? " " : "WELL DONE ! ";
                MessageBox.Show(s, "Fin de partie.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
     
        private void Boutons_Click(object sender, EventArgs e)
        {
            ButtonGame b = sender as ButtonGame;
            if (!b.BackColor.Equals(Color.Red) && cmpt != 0)
                return;
            if (m_life == 0)
            {
                m_solutionButton.Visible=false;
                return;
            }    
            if (m_shot_played.Count != 0)
            {
                ColorClear();
                ButtonGame précédent = m_shot_played.Peek();
                précédent.Text = cmpt.ToString();

            }
            m_RandomButton.Visible = false;
            m_solutionButton.Visible = true;
            m_cancelButton.Visible = true;
            m_ResetButton.Visible = true;

            ++cmpt;
            m_shot_played.Push(b);
            //b.Text = cmpt.ToString();
            b.BackgroundImage = cavalier;
            b.Enabled = false;
            PossibleCases();
        }

        
        private void m_exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Are you sure you want to quit ?", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (r == DialogResult.Yes)
                this.Close();

        }
        private void m_cancelButton_Click(object sender, EventArgs e)
        {
            if (m_shot_played.Count == 0 || m_life == 0)
                return;
            m_life--;
            displayLife();
            if (m_life == 0)
                m_cancelButton.Visible = false;
            ColorClear();
            ButtonGame b = m_shot_played.Pop();
            b.Enabled = true;
            b.Text = "";
            --cmpt;
            if (m_shot_played.Count == 0)
            {
                m_RandomButton.Visible = true;
                m_solutionButton.Visible = false;
                return;
            }
            b = m_shot_played.Pop();
            Color c = b.BackColor;
            b.Enabled = true;
            --cmpt;
            b.Text = "";
            b.BackColor = Color.Red;
            b.PerformClick();
            b.BackColor = c;
        }

        private void ButtonReset_Clik(object sender, EventArgs e)
        {
            if (m_shot_played.Count() != 0)
            {
                ColorClear();
                while (m_shot_played.Count() != 0)
                {
                    ButtonGame b = m_shot_played.Pop();
                    b.Enabled = true;
                    b.Text = "";
                }
            }
            cmpt = 0;
            m_life = 5;
            displayLife();
            m_cancelButton.Visible = false;
            m_RandomButton.Visible = true;
            m_solutionButton.Visible = false;
            m_ResetButton.Visible = false;
        }

        void displayLife()
        {
            label1.Text = m_life > 1 ? "life :" + m_life : m_life == 1 ? "You have one life left." : "You have used up your lives, click on the Reset button";
        }
        private void m_aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Human-Machine Interface project in 2nd year, Instrumentation and Embedded Systems at the Galilée Institute", "HMI", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void m_ruleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on a box to start or on the random button", "Informations", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void m_RandomButton_Click(object sender, EventArgs e)
        {
            if (cmpt != 0)
            {
                MessageBox.Show("Invalid action", "Action", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Random r = new Random();
            int ligne = r.Next(2, 10);
            int colonne = r.Next(2, 10);
            echec[ligne, colonne].PerformClick();

        }
        private void m_solutionButton_Click(object sender, EventArgs e)
        {
            if (m_shot_played.Count == 0)
                return;
            DialogResult r = MessageBox.Show("Would you like to see the solution as a simulation ?", "Solution", MessageBoxButtons.YesNoCancel);
            if (r == DialogResult.Yes)
            {
                ButtonGame b = m_shot_played.Last();
                Simulation f = new Simulation(b.getline(), b.getColumn());
                this.Hide();
                f.ShowDialog();
                this.Close();
            }
        }
    }
}
