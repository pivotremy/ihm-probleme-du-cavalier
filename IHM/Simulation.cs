﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IHM
{
    public partial class Simulation : Form
    {
        private Label label1;
        private Label label2;
        private ListBox Pas;
        private MenuStrip m_menuStrip1;
        private ToolStripMenuItem m_settingToolStripMenuItem;
        private ToolStripMenuItem m_FontAndColorToolStripMenuItem;
        private ToolStripMenuItem m_numberColorToolStripMenuItem;
        private ToolStripMenuItem m_checkerColorToolStripMenuItem;
        private ToolStripMenuItem m_caseOddToolStripMenuItem;
        private ToolStripMenuItem m_casePairToolStripMenuItem;
        private ToolStripMenuItem m_fileToolStripMenuItem;
        private ToolStripMenuItem m_exitToolStripMenuItem;
        private Button m_repriseButton;
        private Button m_pauseButton;
        private Button m_resetButton;
        private Button m_startRandomButton;
        private TrackBar trackBar1;



        Button[,] grille = new Button[8, 8];
        int[,] echec = new int[12, 12];
        Timer m_TimerIHM;
        int nbrePas = 1;
        static readonly int[] depi = new int[] { 2, 1, -1, -2, -2, -1, 1, 2 };
        static readonly int[] depj = new int[] { 1, 2, 2, 1, -1, -2, -2, -1 };
        int[] ligne;
        int[] col;
        int nb_fuite, min_fuite, lmin_fuite = 0;
        int k;
        int starti, startj;
        Boolean abandon;
        Color m_OddColor;
        Color m_PairColor;
        Boolean resetPossible;
        Boolean m_currentStateRuning;
        Image cavalier=Image.FromFile("Pictures\\CavalierR.jpg");

        private void m_startRandomButton_Click(object sender, EventArgs e)
        {
            m_TimerIHM.Enabled = true;
            m_startRandomButton.Enabled = false;
            Random random = new Random();
            int ii = random.Next(0, 7);
            int jj = random.Next(0, 7);
            startSimulation(ii, jj);
        }
        private void startSimulation(int ii, int jj)
        {
            resetPossible = true;
            m_currentStateRuning = true;
            int i, j;
            for (i = 0; i < 12; i++)
                for (j = 0; j < 12; j++)
                    echec[i, j] = ((i < 2 | i > 9 | j < 2 | j > 9) ? -1 : 0);
            i = ii + 2; j = jj + 2;
            ligne[0] = ii;
            col[0] = jj;
            echec[i, j] = 1;


            for (k = 2; k <= 64; k++)
            {
                for (int l = 0, min_fuite = 11; l < 8; l++)
                {
                    ii = i + depi[l]; jj = j + depj[l];

                    nb_fuite = ((echec[ii, jj] != 0) ? 10 : fuite(ii, jj));

                    if (nb_fuite < min_fuite)
                    {
                        min_fuite = nb_fuite; lmin_fuite = l;
                    }
                }
                if (min_fuite == 9 & k != 64)
                {
                    MessageBox.Show("DEAD END");
                    break;
                }
                i += depi[lmin_fuite]; j += depj[lmin_fuite];
                echec[i, j] = k;
                ligne[k - 1] = i - 2;
                col[k - 1] = j - 2;
            }
            k = 0;
            m_TimerIHM.Start();
        }

        public Simulation()
        {
            InitializeComponent();
        }
        public Simulation(int i, int j)
        {
            InitializeComponent();
            starti = i - 2;
            startj = j - 2;
            abandon = true;
        }

        private void Simulation_Load(object sender, EventArgs e)
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Pas = new System.Windows.Forms.ListBox();
            this.m_menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.m_fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_FontAndColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_numberColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_checkerColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_caseOddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_casePairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_repriseButton = new System.Windows.Forms.Button();
            this.m_pauseButton = new System.Windows.Forms.Button();
            this.m_resetButton = new System.Windows.Forms.Button();
            this.m_startRandomButton = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.m_menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();


            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(540, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Speed";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(10+7*60, 9*60-2);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Pas";
            // 
            // Pas
            // 
            this.Pas.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pas.FormattingEnabled = true;
            this.Pas.Items.AddRange(new object[]{"5", "10","64"});
            this.Pas.Location = new System.Drawing.Point(10+7*60, 10+9*60);
            this.Pas.Margin = new System.Windows.Forms.Padding(2);
            this.Pas.Name = "Pas";
            this.Pas.Size = new System.Drawing.Size(54, 43);
            this.Pas.TabIndex = 7;
            this.Pas.SelectedIndexChanged += new System.EventHandler(this.Pas_SelectedIndexChanged);
            // 
            // m_menuStrip1
            // 
            this.m_menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.m_menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.m_menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_fileToolStripMenuItem,
            this.m_settingToolStripMenuItem});
            this.m_menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.m_menuStrip1.Name = "m_menuStrip1";
            this.m_menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.m_menuStrip1.Size = new System.Drawing.Size(806, 24);
            this.m_menuStrip1.TabIndex = 9;
            this.m_menuStrip1.Text = "m_menuStrip1";
            // 
            // m_fileToolStripMenuItem
            // 
            this.m_fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_exitToolStripMenuItem});
            this.m_fileToolStripMenuItem.Name = "m_fileToolStripMenuItem";
            this.m_fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.m_fileToolStripMenuItem.Text = "File";
            // 
            // m_exitToolStripMenuItem
            // 
            this.m_exitToolStripMenuItem.Name = "m_exitToolStripMenuItem";
            this.m_exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.m_exitToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.m_exitToolStripMenuItem.Text = "Exit";
            this.m_exitToolStripMenuItem.Click += new System.EventHandler(this.m_exitToolStripMenuItem_Click);

            // 
            // m_settingToolStripMenuItem
            // 
            this.m_settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_FontAndColorToolStripMenuItem});
            this.m_settingToolStripMenuItem.Name = "m_settingToolStripMenuItem";
            this.m_settingToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.m_settingToolStripMenuItem.Text = "Setting";
            // 
            // m_FontAndColorToolStripMenuItem
            // 
            this.m_FontAndColorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_numberColorToolStripMenuItem,
            this.m_checkerColorToolStripMenuItem});
            this.m_FontAndColorToolStripMenuItem.Name = "m_FontAndColorToolStripMenuItem";
            this.m_FontAndColorToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.m_FontAndColorToolStripMenuItem.Text = "Font and Color";
            // 
            // m_numberColorToolStripMenuItem
            // 
            this.m_numberColorToolStripMenuItem.Name = "m_numberColorToolStripMenuItem";
            this.m_numberColorToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.m_numberColorToolStripMenuItem.Text = "number color";
            this.m_numberColorToolStripMenuItem.Click += new System.EventHandler(this.m_numberColorToolStripMenuItem_Click);
            // 
            // m_checkerColorToolStripMenuItem
            // 
            this.m_checkerColorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_caseOddToolStripMenuItem,
            this.m_casePairToolStripMenuItem});
            this.m_checkerColorToolStripMenuItem.Name = "m_checkerColorToolStripMenuItem";
            this.m_checkerColorToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.m_checkerColorToolStripMenuItem.Text = "chessboard color";
            // 
            // m_caseOddToolStripMenuItem
            // 
            this.m_caseOddToolStripMenuItem.Name = "m_caseOddToolStripMenuItem";
            this.m_caseOddToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.m_caseOddToolStripMenuItem.Text = "Odd box";
            this.m_caseOddToolStripMenuItem.Click += new System.EventHandler(this.m_caseOddToolStripMenuItem_Click);
            // 
            // m_casePairToolStripMenuItem
            // 
            this.m_casePairToolStripMenuItem.Name = "m_casePairToolStripMenuItem";
            this.m_casePairToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.m_casePairToolStripMenuItem.Text = "Even boxes";
            this.m_casePairToolStripMenuItem.Click += new System.EventHandler(this.m_casePairToolStripMenuItem_Click);
            // 
            // m_repriseButton
            // 
            this.m_repriseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_repriseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_repriseButton.Font = new System.Drawing.Font("Charlemagne Std", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_repriseButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_repriseButton.Location = new System.Drawing.Point(150, 540);
            this.m_repriseButton.Margin = new System.Windows.Forms.Padding(2);
            this.m_repriseButton.Name = "m_repriseButton";
            this.m_repriseButton.Size = new System.Drawing.Size(80, 60);
            this.m_repriseButton.TabIndex = 4;
            this.m_repriseButton.Text = "REPRISE";
            this.m_repriseButton.UseVisualStyleBackColor = false;
            this.m_repriseButton.Click += new System.EventHandler(this.m_repriseButton_Click);
            // 
            // m_pauseButton
            // 
            this.m_pauseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_pauseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_pauseButton.Font = new System.Drawing.Font("Charlemagne Std", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_pauseButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_pauseButton.Location = new System.Drawing.Point(50, 540);
            this.m_pauseButton.Margin = new System.Windows.Forms.Padding(2);
            this.m_pauseButton.Name = "m_pauseButton";
            this.m_pauseButton.Size = new System.Drawing.Size(80, 60);
            this.m_pauseButton.TabIndex = 3;
            this.m_pauseButton.Text = "PAUSE";
            this.m_pauseButton.UseVisualStyleBackColor = false;
            this.m_pauseButton.Click += new System.EventHandler(this.m_pauseButton_Click);
            // 
            // m_resetButton
            // 
            this.m_resetButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_resetButton.Font = new System.Drawing.Font("Charlemagne Std", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_resetButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_resetButton.Location = new System.Drawing.Point(240, 540);
            this.m_resetButton.Margin = new System.Windows.Forms.Padding(2);
            this.m_resetButton.Name = "m_resetButton";
            this.m_resetButton.Size = new System.Drawing.Size(80, 60);
            this.m_resetButton.TabIndex = 1;
            this.m_resetButton.Text = "RESET";
            this.m_resetButton.UseVisualStyleBackColor = false;
            this.m_resetButton.Click += new System.EventHandler(this.m_resetButton_Click);
            // 
            // m_startRandomButton
            // 
            this.m_startRandomButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_startRandomButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_startRandomButton.Font = new System.Drawing.Font("Charlemagne Std", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_startRandomButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_startRandomButton.Location = new System.Drawing.Point(340, 540);
            this.m_startRandomButton.Margin = new System.Windows.Forms.Padding(2);
            this.m_startRandomButton.Name = "m_startRandomButton";
            this.m_startRandomButton.Size = new System.Drawing.Size(83, 61);
            this.m_startRandomButton.TabIndex = 0;
            this.m_startRandomButton.Text = "RANDOM";
            this.m_startRandomButton.UseVisualStyleBackColor = false;
            this.m_startRandomButton.Click += new System.EventHandler(this.m_startRandomButton_Click);

            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.trackBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.trackBar1.LargeChange = 300;
            this.trackBar1.Location = new System.Drawing.Point(540, 59);
            this.trackBar1.Maximum = 3000;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(163, 45);
            this.trackBar1.SmallChange = 100;
            this.trackBar1.TabIndex = 12;
            this.trackBar1.Scroll += new System.EventHandler(this.TrackBar1_Scroll);

            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 623);
            this.Controls.Add(this.m_menuStrip1);
            this.Controls.Add(this.m_pauseButton);
            this.Controls.Add(this.m_repriseButton);
            this.Controls.Add(this.m_resetButton);
            this.Controls.Add(this.m_startRandomButton);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pas);
            this.Controls.Add(this.label2);

            this.MainMenuStrip = this.m_menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simulation";
            this.m_menuStrip1.ResumeLayout(false);
            this.m_menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            this.BackgroundImageLayout = ImageLayout.Stretch;
            m_TimerIHM = new Timer();
            m_TimerIHM.Enabled = false;
            m_TimerIHM.Tick += new EventHandler(TimerEventProcessor);
            trackBar1.Value = trackBar1.Maximum / 2;
            m_TimerIHM.Interval = trackBar1.Maximum / 2;
            ligne = new int[64];
            col = new int[64];
            m_PairColor = Color.Gray;
            m_OddColor = Color.White;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)

                {
                    Button b = new Button();
                    this.Controls.Add(b);
                    Point p = new Point(50 + 60 * i, 50 + 60 * j);
                    b.Location = p;
                    Size s = new Size(60, 60);
                    b.Size = s;
                    Boolean blanc;
                    b.Click += new EventHandler(this.StartCase);
                    blanc = i % 2 == j % 2 ? true : false;
                    b.BackColor = blanc ? m_OddColor : m_PairColor;
                    b.Font = new Font("Impact", 15);
                    grille[i, j] = b;
                }
            if (abandon)
            {
                startSimulation(starti, startj);
                m_startRandomButton.Enabled = false;
                return;
            }
            this.Show();
            this.Focus();
            MessageBox.Show("To start, click on a box or the RANDOM button !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void m_resetButton_Click(object sender, EventArgs e)
        {
            if (!resetPossible)
                return;
            m_TimerIHM.Stop();
            DialogResult r = MessageBox.Show("Do you want to replay the last simulation ?", "Reset", MessageBoxButtons.YesNoCancel);
            switch (r)
            {
                case DialogResult.Yes:
                    k = 0;
                    foreach (Button b in grille)
                    {
                        b.Text = "";
                        b.Image = null;
                    }
                    m_TimerIHM.Start();
                    break;
                case DialogResult.No:
                    foreach (Button b in grille)
                    {
                        b.Text = "";
                        b.Image = null;
                    }
                    m_startRandomButton.Enabled = true;
                    resetPossible = false;
                    m_currentStateRuning = false;
                    break;
                default:
                    m_TimerIHM.Start();
                    break;
            }
        }

        private void m_pauseButton_Click(object sender, EventArgs e)
        {
            m_TimerIHM.Stop();
        }

        private void m_repriseButton_Click(object sender, EventArgs e)
        {
            if (m_currentStateRuning)
                m_TimerIHM.Start();
        }


        private void Pas_SelectedIndexChanged(object sender, EventArgs e)
        {
            nbrePas = int.Parse(Pas.SelectedItem.ToString());
        }

        private void m_numberColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            c.ShowDialog();
            Color choice = c.Color;
            foreach (Button b in grille)
                b.ForeColor = choice;
        }

        private void m_caseOddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            c.ShowDialog();
            m_OddColor = c.Color;
            colorierGrille();

        }
        private void colorierGrille()
        {
            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    Button b = grille[i, j];
                    Boolean color = i % 2 == j % 2 ? true : false;
                    b.BackColor = color ? m_OddColor : m_PairColor;
                }
            }
        }
        private void m_casePairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            c.ShowDialog();
            m_PairColor = c.Color;
            colorierGrille();
        }

        private void m_exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TrackBar1_Scroll(object sender, EventArgs e)
        {
            int v = (trackBar1.Maximum - trackBar1.Value) + 100;

            m_TimerIHM.Interval = v;
        }

        private void StartCase(Object sender, EventArgs e)
        {
            if (m_currentStateRuning)
                return;
            m_startRandomButton.Enabled = false;
            Button choix = sender as Button;
            for (int i = 0; i < 8; ++i)
                for (int j = 0; j < 8; ++j)
                {
                    if (grille[i, j].Equals(choix))
                    {
                        startSimulation(i, j);
                        return;
                    }
                }

        }
        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            if (k >= ligne.Length || !m_currentStateRuning)
            {
                m_TimerIHM.Stop();
                return;
            }
            for (int n = 0; k < ligne.Length && n < nbrePas; ++n)
            {
                int i = ligne[k];
                int j = col[k];
                grille[i, j].Image = cavalier;
                if (k != 0)
                {
                    i = ligne[k - 1];
                    j = col[k - 1];
                    grille[i, j].Image = null;
                    grille[i, j].Text = k.ToString();
                }
                ++k;
            }
        }
        private int fuite(int i, int j)
        {
            int n, l;

            for (l = 0, n = 8; l < 8; l++)
                if (echec[i + depi[l], j + depj[l]] != 0) n--;

            return (n == 0) ? 9 : n;
        }
    }
}
