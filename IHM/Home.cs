﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IHM
{
    public partial class Home : Form
    {
        private Button m_buttonGame;
        private Button m_buttonSimulation;
        private Label m_labelHome;
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            this.m_buttonGame       = new Button();
            this.m_buttonSimulation = new Button();
            this.m_labelHome        = new Label();
            // m_buttonGame
            this.m_buttonGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.m_buttonGame.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m_buttonGame.Font      = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_buttonGame.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_buttonGame.Location  = new System.Drawing.Point(147, 131);
            this.m_buttonGame.Margin    = new System.Windows.Forms.Padding(2);
            this.m_buttonGame.Name      = "m_buttonGame";
            this.m_buttonGame.Size      = new System.Drawing.Size(131, 81);
            this.m_buttonGame.TabIndex  = 0;
            this.m_buttonGame.Text      = "Game";
            this.m_buttonGame.UseVisualStyleBackColor = false;
            this.m_buttonGame.Click                  += new System.EventHandler(this.buttonMenu_Click);
            // m_buttonSimulation
            this.m_buttonSimulation.BackColor   = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.m_buttonSimulation.FlatStyle   = System.Windows.Forms.FlatStyle.Popup;
            this.m_buttonSimulation.Font        = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_buttonSimulation.ForeColor   = System.Drawing.SystemColors.ButtonHighlight;
            this.m_buttonSimulation.Location    = new System.Drawing.Point(521, 131);
            this.m_buttonSimulation.Margin      = new System.Windows.Forms.Padding(2);
            this.m_buttonSimulation.Name        = "m_buttonSimulation";
            this.m_buttonSimulation.Size        = new System.Drawing.Size(137, 81);
            this.m_buttonSimulation.TabIndex    = 1;
            this.m_buttonSimulation.Text        = "Simulation";
            this.m_buttonSimulation.UseVisualStyleBackColor = false;
            this.m_buttonSimulation.Click                   += new System.EventHandler(this.buttonMenu_Click);
            // m_labelHome
            this.m_labelHome.AutoSize = true;
            this.m_labelHome.BackColor = System.Drawing.Color.Transparent;
            this.m_labelHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_labelHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_labelHome.Location = new System.Drawing.Point(235, 62);
            this.m_labelHome.Name = "m_labelHome";
            this.m_labelHome.Size = new System.Drawing.Size(281, 20);
            this.m_labelHome.TabIndex = 2;
            this.m_labelHome.Text = "Welcome to Euler's horseman game ";
            // Form3
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.m_labelHome);
            this.Controls.Add(this.m_buttonSimulation);
            this.Controls.Add(this.m_buttonGame);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home | LOUA PIVOT REMY -2021";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private void buttonMenu_Click(object sender, EventArgs e)
        {
            if(sender is Button)
            {
                Button b = sender as Button;
                if(b.Text.Equals("Game"))
                {
                    Game f = new Game();
                    f.Show();
                }
                if(b.Text.Equals("Simulation"))
                {
                    Simulation f = new Simulation();
                    f.Show();
                }
            }
        }
    }
}
